#ifndef _NGX_POSTGRES_DDEBUG_H_
#define _NGX_POSTGRES_DDEBUG_H_

#include <ngx_core.h>

#if defined(DDEBUG) && (DDEBUG) && (NGX_HAVE_VARIADIC_MACROS)
#define dd(...) fprintf(stderr, "(ngx_postgres) === %s: ", __func__); \
                fprintf(stderr, __VA_ARGS__); \
                fprintf(stderr, " - [%s:%d]\n", __FILE__, __LINE__)
#else
static inline void dd() {}
#endif

#endif
