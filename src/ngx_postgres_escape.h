#ifndef _NGX_POSTGRES_ESCAPE_H_
#define _NGX_POSTGRES_ESCAPE_H_

#include <ngx_core.h>
#include <ngx_http.h>


void       ngx_postgres_escape_string(ngx_http_script_engine_t *);

#endif
