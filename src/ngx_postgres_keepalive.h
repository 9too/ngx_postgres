#ifndef _NGX_POSTGRES_KEEPALIVE_H_
#define _NGX_POSTGRES_KEEPALIVE_H_

#include <ngx_core.h>
#include <ngx_http.h>
#include <libpq-fe.h>

#include "ngx_postgres_module.h"
#include "ngx_postgres_upstream.h"


typedef struct {
    ngx_queue_t                        queue;
    ngx_postgres_upstream_srv_conf_t  *srv_conf;
    ngx_connection_t                  *connection;
    PGconn                            *pgconn;
    struct sockaddr                    sockaddr;
    socklen_t                          socklen;
    ngx_str_t                          name;
} ngx_postgres_keepalive_cache_t;


ngx_int_t   ngx_postgres_keepalive_init(ngx_pool_t *,
                ngx_postgres_upstream_srv_conf_t *);
ngx_int_t   ngx_postgres_keepalive_get_peer_single(ngx_peer_connection_t *,
                ngx_postgres_upstream_peer_data_t *,
                ngx_postgres_upstream_srv_conf_t *);
ngx_int_t   ngx_postgres_keepalive_get_peer_multi(ngx_peer_connection_t *,
                ngx_postgres_upstream_peer_data_t *,
                ngx_postgres_upstream_srv_conf_t *);
void        ngx_postgres_keepalive_free_peer(ngx_peer_connection_t *,
                ngx_postgres_upstream_peer_data_t *,
                ngx_postgres_upstream_srv_conf_t *, ngx_uint_t);
void        ngx_postgres_keepalive_dummy_handler(ngx_event_t *);
void        ngx_postgres_keepalive_close_handler(ngx_event_t *);
void        ngx_postgres_keepalive_cleanup(void *);

#endif
