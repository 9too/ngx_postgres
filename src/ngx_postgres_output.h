#ifndef _NGX_POSTGRES_OUTPUT_H_
#define _NGX_POSTGRES_OUTPUT_H_

#include <ngx_http.h>
#include <libpq-fe.h>


ngx_int_t
ngx_postgres_output_value
(
    ngx_http_request_t *,
    PGresult *
);


ngx_int_t
ngx_postgres_output_text
(
    ngx_http_request_t *,
    PGresult *
);


ngx_int_t
ngx_postgres_output_chain
(
    ngx_http_request_t *,
    ngx_chain_t *
);

#endif
