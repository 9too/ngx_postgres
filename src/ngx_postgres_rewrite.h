#ifndef _NGX_POSTGRES_REWRITE_H_
#define _NGX_POSTGRES_REWRITE_H_

#include <ngx_core.h>
#include <ngx_http.h>

#include "ngx_postgres_module.h"


ngx_int_t  ngx_postgres_rewrite(ngx_http_request_t *,
               ngx_postgres_rewrite_conf_t *);
ngx_int_t  ngx_postgres_rewrite_changes(ngx_http_request_t *,
               ngx_postgres_rewrite_conf_t *);
ngx_int_t  ngx_postgres_rewrite_rows(ngx_http_request_t *,
               ngx_postgres_rewrite_conf_t *);

#endif
