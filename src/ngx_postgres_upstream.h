#ifndef _NGX_HTTP_UPSTREAM_POSTGRES_H_
#define _NGX_HTTP_UPSTREAM_POSTGRES_H_

#include <ngx_core.h>
#include <ngx_http.h>
#include <libpq-fe.h>

#include "ngx_postgres_module.h"


typedef enum {
    state_db_connect,
    state_db_send_query,
    state_db_get_result,
    state_db_get_ack,
    state_db_idle
} ngx_postgres_state_t;

typedef struct {
    ngx_postgres_upstream_srv_conf_t  *srv_conf;
    ngx_postgres_loc_conf_t           *loc_conf;
    ngx_http_upstream_t               *upstream;
    ngx_http_request_t                *request;
    PGconn                            *pgconn;
    ngx_postgres_state_t               state;
    ngx_str_t                          query;
    ngx_str_t                          name;
    struct sockaddr                    sockaddr;
    unsigned                           failed;
} ngx_postgres_upstream_peer_data_t;


ngx_int_t   ngx_postgres_upstream_init(ngx_conf_t *,
                ngx_http_upstream_srv_conf_t *);
ngx_int_t   ngx_postgres_upstream_init_peer(ngx_http_request_t *,
                ngx_http_upstream_srv_conf_t *);
ngx_int_t   ngx_postgres_upstream_get_peer(ngx_peer_connection_t *, void *);
void        ngx_postgres_upstream_free_peer(ngx_peer_connection_t *, void *,
                ngx_uint_t);
ngx_flag_t  ngx_postgres_upstream_is_my_peer(const ngx_peer_connection_t *);
void        ngx_postgres_upstream_free_connection(ngx_log_t *,
                ngx_connection_t *, PGconn *,
                ngx_postgres_upstream_srv_conf_t *);


#endif
